import Vue from 'vue'
import Router from 'vue-router'
import VCalendar from 'v-calendar'
import VeeValidate from 'vee-validate'
import SignIn from '@/components/SignIn'
import SignUp from '@/components/SignUp'
import Profile from '@/components/Profile'
import Home from '@/components/Home'
import Menu from '@/components/Menu'
import Cart from '@/components/Cart'

Vue.use(Router);
Vue.use(VCalendar, { componentPrefix: 'vc' });
Vue.use(VeeValidate);
//admin
import Stock from '@/components/Admin/Stock'
import Order from '@/components/Admin/Order'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/profile/:userid',
      name: 'Profile',
      component: Profile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/menu/:type',
      name: 'Menu',
      component: Menu,
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart,
      meta: {
        requiresAuth: true
      }
    },
    //admin
    {
      path: '/admin/stock',
      name: 'Stock',
      component: Stock,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/admin/order',
      name: 'Order',
      component: Order,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

// Check required authentication
router.beforeEach((to, from, next) => {
  var currentUser = firebase.auth().currentUser;
  var requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (to.path === '/home' || to.path === '/menu/pancake' || to.path === '/menu/toast' || to.path === '/menu/waffle' || to.path === '/menu/crepe' || to.path === '/menu/beverage') {
    next();
  } else if (requiresAuth && !currentUser) {
    next('/home');
  } else if (!requiresAuth && currentUser) {
    next('/home');
  } else {
    next();
  }
});

export default router;